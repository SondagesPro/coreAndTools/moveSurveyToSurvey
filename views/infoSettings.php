<div class="col-sm-6 col-sm-offset-4">
    <div class="panel panel-default">
    <div class="panel-heading" id="listSurveyToSurveyTitle" role="button" data-toggle="collapse" href="#listSurveyToSurvey" aria-expanded="true" aria-controls="listSurveyToSurvey">
        <?php printf($lang['%s survey(s) to be moved (click to see the list)'],$countSurvey) ?>
    </div>
    <div id="listSurveyToSurvey" class="panel-collapse collapse" role="tabpanel" aria-labelledby="listSurveyToSurveyTitle">
        <div class="panel-body">
            <pre><?php foreach($listSurveys as $from => $to) {
                    echo "$from \t $to \n";
                } ?></pre>
        </div>
    </div>
    </div>
</div>
