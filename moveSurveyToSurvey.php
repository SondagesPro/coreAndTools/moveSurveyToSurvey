<?php
/**
 * moveSurveyToSurvey : Allow top move survey to other survey via CLI/direct event
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020 Denis Chenu <https://www.sondages.pro>
 * @license AGPL v3
 * @version 0.2.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class moveSurveyToSurvey extends PluginBase
{
    protected $storage = 'DbStorage';
    static protected $description = 'Allow to move surveys to another survey, respect token and response';
    static protected $name = 'moveSurveyToSurvey';

    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'alertPlugin' => array(
            'type' => 'info',
            'content' => "",
        ),
        'localDirectoryForBackup' => array(
            'type' => 'string',
            'label' => 'Local directory for backup your surveys as LSA',
            'help' => 'This directory must be out of web access, but web user must be allowed to create directory and write in this directory',
            'default' => '',
        ),
        'noBackup' => array(
            'type' => 'checkbox',
            'value' => 1,
            'htmlOptions'=>array(
                'uncheckValue' => 0
            ),
            'label'=> 'Move and delete survey even without backup directory.',
            'help'=> 'When move survey to another survey, last action done was deletion of survey. Then you need to confirm if you want to do it without backup.',
            'default'=>0,
        ),
        'checkTokenTable' => array(
            'type' => 'checkbox',
            'value' => 1,
            'htmlOptions'=>array(
                'uncheckValue' => 0
            ),
            'label' => 'Check if destination has token table if origin have it.',
            'help' => 'If you want to copy a survey with token table inside a survey without : survey copy and deletion was skipped.',
            'default' => 1,
        ),
        'textTokenSeparator' => array(
            'type' => 'string',
            'htmlOptions'=>array(
                'maxlength' => 1
            ),
            'label' => 'Caracter to separate surey id and token in tokan table and column.',
            'help' => 'To be sure to have unicity on token: system add survey before token, you can use a separator. The final value was cut to 35 caracter.',
            'default' => "#",
        ),
        'minimalResponses' => array(
            'type' => 'int',
            'htmlOptions'=>array(
                'min' => 0
            ),
            'label' => 'Minimum number of common questions between the 2 surveys.',
            'help' => 'If you want to copy a survey with less question correspondance with destintaion survey : survey copy and deletion was skipped. If origin survey have less question than this setting : all question must have commin question in destination survey.',
            'default' => 1,
        ),
        'newSidToSid' => array(
            'type' => 'text',
            'value' => '',
            'htmlOptions'=>array(
                'disabled' => 'disabled'
            ),
            'label'=> 'Add survey to survey line.',
            'help'=> '2 survey id by line separated by tab or ;. 1id is the from survey, 2nd is the to survey',
            'default'=>"",
        ),
        'currentSidToSid' => array(
            'type' => 'info',
            'content' => "",
        ),
        'resetSidToSid' => array(
            'type' => 'checkbox',
            'value' => 1,
            'htmlOptions'=>array(
                'uncheckValue' => 0,
                'disabled' => 'disabled'
            ),
            'label'=> 'Reset the survey to be moved.',
            'help'=> 'This delete all the lines.',
            'default'=>0,
        ),
        'infoAction' => array(
            'type' => 'info',
            'class' => 'well',
            'content' => "",
        ),
    );

    /* const interger */
    static private $dbVersion= 2;
    /* @var string */
    private $errorOnDir = "";

    /**
     * @var int debug (for echoing on terminal) 0=>ERROR, 1=>WARNING+BASICINFORMATION, 2=>INFO, 3=>DEBUG/TRACE
     */
    public $debug = 2;

    /**
    * @see parent::saveSettings()
    */
    public function saveSettings($settings)
    {
        if(!Permission::model()->hasGlobalPermission('settings','update')) {
            throw new CHttpException(403);
        }
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        if(!empty($settings['localDirectoryForBackup']))
        {
            $checkLocalDirectory = $settings['localDirectoryForBackup'];
            $settings['localDirectoryForBackup'] = $this->checkLocalDirectory($settings['localDirectoryForBackup']);
            if($settings['localDirectoryForBackup'] && $settings['localDirectoryForBackup'] != $checkLocalDirectory) {
                App()->setFlashMessage($this->gT("The directory was updated to the real path."),0);
            }
            if(!$settings['localDirectoryForBackup']) {
                App()->setFlashMessage($this->errorOnDir,'danger');
            }
        }
        if(!empty($settings['resetSidToSid']) && $this->isActive() ) {
            $deleted = \moveSurveyToSurvey\models\surveyToSurvey::model()->deleteAll();
            Yii::app()->setFlashMessage(sprintf($this->gt("%s line(s) deleted"),$deleted),'info');
        }
        if(!empty($settings['newSidToSid']) && $this->isActive() )
        {
            $count = 0;
            $aSidsToSids = preg_split('/\r\n|\r|\n/', $settings['newSidToSid'], -1, PREG_SPLIT_NO_EMPTY);
            foreach($aSidsToSids as $sidToSid) {
                $aSids = preg_split('/\t|;/', $sidToSid, -1, PREG_SPLIT_NO_EMPTY);
                if(count($aSids) == 2) {
                    if($this->addSidToSid($aSids[0],$aSids[1])) {
                        $count ++;
                    }
                }
            }
            Yii::app()->setFlashMessage(sprintf($this->gt("%s line(s) added"),$count),'info');
        }
        $settings['resetSidToSid'] = null;
        $settings['newSidToSid'] = null;
        parent::saveSettings($settings);
    }
    
    /** @inheritdoc **/
    public function beforeActivate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->setDb();
    }

    /**
     * Add a sid to sid line
     * @param integer|string from survey
     * @param integer|string to survey
     * @return integre : number of line to be added
     */
    private function addSidToSid($fromSid,$toSid) {
        $fromSid = trim($fromSid);
        $toSid = trim($toSid);
        $oSurveyToSurvey = \moveSurveyToSurvey\models\surveyToSurvey::model()->findByPk(array('fromsid'=>$fromSid,'tosid'=>$toSid));
        if($oSurveyToSurvey) {
            return 0;
        }
        if(!$this->checkIfSurveyIsValid($fromSid)) {
            return 0;
        }
        if(!$this->checkIfSurveyIsValid($toSid)) {
            return 0;
        }
        $oSurveyToSurvey = new \moveSurveyToSurvey\models\surveyToSurvey;
        $oSurveyToSurvey->fromsid = $fromSid;
        $oSurveyToSurvey->tosid = $toSid;
        if($oSurveyToSurvey->save()) {
            return 1;
        }
        return 0;
    }

    /**
     * Check if a survey is valid for move in/out 
     * @param integer from survey
     * @return boolean
     */
    private function checkIfSurveyIsValid($sid)
    {
        $oSurvey = Survey::model()->findByPk($sid);
        if(!$oSurvey) {
            return false;
        }
        if($oSurvey->active != "Y") {
            return false;
        }
        return true;
    }
    /*
     * Set default when show setting
     */
    public function getPluginSettings($getValues=true)
    {
        if(!Permission::model()->hasGlobalPermission('settings','read')) {
            throw new CHttpException(403);
        }
        $this->setDb();
        $aPluginSettings = parent::getPluginSettings($getValues);
        if($this->isActive()) {
            $aPluginSettings['newSidToSid']['htmlOptions']['disabled']= null;
            $aPluginSettings['resetSidToSid']['htmlOptions']['disabled']= null;
        }
        $aPluginSettings['localDirectoryForBackup']['help'] = CHtml::tag("p",array(),$this->gT('This directory must be out of web access, but web user must be allowed to create directory and write in this directory.'));
        $aPluginSettings['localDirectoryForBackup']['help'].= CHtml::tag("p",array(),sprintf($this->gT('The directory of Limesurvey is : %s'),App()->getConfig('rootdir')));
        if($getValues && !empty($aPluginSettings['localDirectory']['current']) ) {
            $checkLocalDirectory = $aPluginSettings['localDirectory']['current'];
            $settings['localDirectoryForBackup'] = $this->checkDirectory($aPluginSettings['localDirectory']['current']);
            if($aPluginSettings['localDirectoryForBackup']['current'] && $aPluginSettings['localDirectory']['current'] != $checkLocalDirectory) {
                App()->setFlashMessage($this->gT("The directory was updated to the real path."),0);
            }
            if(!$aPluginSettings['localDirectoryForBackup']['current']) {
                App()->setFlashMessage($this->errorOnDir,'danger');
            }
        }

        if($this->isActive()) {
            Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
            $oSurveysToSurveys = \moveSurveyToSurvey\models\surveyToSurvey::model()->findAll(array('order'=>'tosid'));
            if(empty($oSurveysToSurveys)) {
                $aPluginSettings['currentSidToSid']['content'] = "No survey to move in database";
            } else {
                $listSurveys = CHtml::listData($oSurveysToSurveys,'fromsid','tosid');
                $aData = array(
                    'lang' => array(
                        "%s survey(s) to be moved (click to see the list)" => $this->gT("%s survey(s) to be moved (click to see the list)"),
                    ),
                    'listSurveys' => $listSurveys,
                    'countSurvey' => count($oSurveysToSurveys)
                );
                $content = Yii::app()->getController()->renderPartial(get_class($this).".views.infoSettings",$aData,true);
                $aPluginSettings['currentSidToSid']['content'] = $content;
            }
        }

        $haveGetQuestionInformation = Yii::getPathOfAlias('getQuestionInformation');
        if(!$haveGetQuestionInformation) {
            $aPluginSettings['alertPlugin']['content'] = CHtml::tag("p",array(),sprintf($this->gT("Unable to use this plugin, you need %s plugin."),CHtml::link("getQuestionInformation","https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation")));
            $aPluginSettings['alertPlugin']['class'] = 'alert alert-danger';
        }
        // infoAction
        $content = "<p>".$this->gT("Action to do via PHP cli. Inside LimeSurvey directory.")."</p>";
        $content .= "<ul>";
        $content .= "<li><code>php application/commands/console.php plugin --target=moveSurveyToSurvey</code></li>";
        $content .= "<li><code>php application/commands/console.php plugin cron moveSurveyToSurvey</code></li>";
        $content .= "</ul>";
        $aPluginSettings['infoAction']['content'] = $content;
        return $aPluginSettings;
    }

    /**
    * Add function to be used in cron event
    * @see parent::init()
    */
    public function init()
    {
        $this->subscribe('beforeActivate','beforeActivate');
        /* Action on cron */
        $this->subscribe('cron','moveSurveysCron');
        /* Action on direct */
        $this->subscribe('direct','moveSurveysCli');
    }

    public function moveSurveysCron()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $args = isset($_SERVER['argv']) ? $_SERVER['argv']:array();
        if(!in_array('moveSurveyToSurvey',$args)) {
            return;
        }
        $this->moveSurveys();
    }

    public function moveSurveysCli()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if($this->event->get("target") != get_class()) {
            return;
        }
        $this->moveSurveys();
    }

    private function moveSurveys()
    {
        $this->LsCommandFix();
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $localDirectoryForBackup = $this->get('localDirectoryForBackup');
        $noBackup = $this->get('noBackup');
        if(empty($localDirectoryForBackup) && empty($noBackup)) {
            throw new Exception("No directory for backup set, no backup not confirmed.");
        }
        $this->setDb();
        if($localDirectoryForBackup) {
            $localDirectoryForBackup = $this->checkLocalDirectory($localDirectoryForBackup);
            if(empty($localDirectoryForBackup)) {
                throw new Exception("Directory ".$this->get('localDirectoryForBackup')." is invalid.");
            }
            Yii::import(get_class($this).'.helpers.globalsettings_helper', true);
            /** Needed function for export**/
            Yii::import('application.helpers.export_helper', true);
            /* lsa is zip */
            if (!defined('PCLZIP_TEMPORARY_DIR')) {
                define('PCLZIP_TEMPORARY_DIR', $localDirectoryForBackup.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR);
            }
            Yii::import('application.libraries.admin.pclzip', true);
        }
        $haveGetQuestionInformation = Yii::getPathOfAlias('getQuestionInformation');
        if(!$haveGetQuestionInformation) {
            throw new Exception("Need getQuestionInformation plugin.");
        }
        $checkTokenTable = $this->get('checkTokenTable',null,null,1);
        $minimalResponses = $this->get('minimalResponses',null,null,1);
        $separator = $this->get('textTokenSeparator',null,null,"#");
        $aCommonsColumns = array(
            'token',
            'seed',
            'lastpage',
            'startlanguage',
            'startdate',
            'datestamp',
            'submitdate',
            'ipaddress',
            'url',
        );
        $oSurveysToSurveys = \moveSurveyToSurvey\models\surveyToSurvey::model()->findAll(array('order'=>'tosid, fromsid'));
        $currentTo = null;
        $aToResponseAttributes = array();
        $aToCommonsColumns = array();
        $aToQuestionsColumns = array();
        foreach($oSurveysToSurveys as $oSurveyToSurvey) {
            $this->moveSurveyToSurveyLog("Start move survey {$oSurveyToSurvey->fromsid} to {$oSurveyToSurvey->tosid}",1);
            /* Information on destination survey */
            if($currentTo != $oSurveyToSurvey->tosid) {
                $oSurveyTo = Survey::model()->findByPk($oSurveyToSurvey->tosid);
                if(empty($oSurveyTo)) {
                    $this->moveSurveyToSurveyLog("Invalid to survey {$oSurveyToSurvey->tosid} for {$oSurveyToSurvey->fromsid}",0);
                    \moveSurveyToSurvey\models\surveyToSurvey::model()->deleteByPk($oSurveyToSurvey->fromsid);
                    continue;
                }
                if($oSurveyTo->active != "Y") {
                    $this->moveSurveyToSurveyLog("Invalid to survey {$oSurveyToSurvey->tosid} for {$oSurveyToSurvey->fromsid} (not activated)",0);
                    \moveSurveyToSurvey\models\surveyToSurvey::model()->deleteByPk($oSurveyToSurvey->fromsid);
                    continue;
                }
                $currentTo = $oSurveyToSurvey->tosid;
                $aToTokenAttributes = array();
                if($this->surveyHasTokens($oSurveyToSurvey->tosid)) {
                    $oToken = \Token::create($oSurveyToSurvey->tosid);
                    $aToTokenAttributes = $oToken->attributes;
                    unset($aToTokenAttributes['tid']);
                    unset($aToTokenAttributes['email']); // Adding on a different scenario
                    unset($aToTokenAttributes['token']); // Need update
                }
                $oResponse = \Response::create($oSurveyToSurvey->tosid);
                $aToResponseAttributes = array_keys($oResponse->attributes);
                $aToCommonsColumns = array_intersect($aCommonsColumns,$aToResponseAttributes);
                $aToColumnsCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($oSurveyToSurvey->tosid);
                $aToCodeColumns = array_flip($aToColumnsCode);
            }
            /* OK : validate from */
            $fromSid = $oSurveyToSurvey->fromsid;
            $oSurveyFrom = Survey::model()->findByPk($fromSid);
            if(empty($oSurveyFrom)) {
                $this->moveSurveyToSurveyLog("Invalid survey {$fromSid} (not exist)",0);
                \moveSurveyToSurvey\models\surveyToSurvey::model()->deleteByPk($fromSid);
                continue;
            }
            if($oSurveyFrom->active != "Y") {
                $this->moveSurveyToSurveyLog("Invalid survey {$fromSid} (not activated)",0);
                \moveSurveyToSurvey\models\surveyToSurvey::model()->deleteByPk($fromSid);
                continue;
            }
            /* Validate number of question code */
            $aFromColumnsCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($fromSid);
            $minToCheck = min($minimalResponses, count($aFromColumnsCode));
            if($minToCheck > 0) {
                $aCommonQuestion = array_intersect_key(array_flip($aFromColumnsCode),$aToCodeColumns);
                if(count($aCommonQuestion) < $minToCheck) {
                    $this->moveSurveyToSurveyLog("Invalid to survey {$oSurveyToSurvey->tosid} for {$oSurveyToSurvey->fromsid} : ".count($aCommonQuestion)." common questions",0);
                    \moveSurveyToSurvey\models\surveyToSurvey::model()->deleteByPk($oSurveyToSurvey->fromsid);
                    continue;
                }
            }
            if($this->surveyHasTokens($fromSid)) {
                if(empty($aToTokenAttributes) && $checkTokenTable) {
                    $this->moveSurveyToSurveyLog("Invalid to survey {$oSurveyToSurvey->tosid} for {$oSurveyToSurvey->fromsid} : no token table",0);
                    \moveSurveyToSurvey\models\surveyToSurvey::model()->deleteByPk($oSurveyToSurvey->fromsid);
                    continue;
                }
                if($this->surveyHasTokens($oSurveyToSurvey->tosid)) {
                    $oFromTokens = \Token::model($fromSid)->findAll(array('order' => 'tid'));
                    $countTokens = 0;
                    foreach($oFromTokens as $oFromToken) {
                        $oToken = \Token::create($currentTo);
                        if(!empty($oFromToken->token)) {
                            $oToken->token = substr($fromSid.$separator.$oFromToken->token,-36);
                            /* @todo : check if already created */
                        } else {
                            $oToken->token = "";
                        }
                        $oToken->save();
                        $oToken->scenario = 'allowinvalidemail';
                        $oToken->email = $oFromToken->email;
                        $oToken->save();
                        $aAttributesToSet = array_intersect_key($oFromToken->getAttributes(),$aToTokenAttributes);
                        $oToken->scenario = 'FinalSubmit';
                        $oToken->setAttributes($aAttributesToSet);
                        $oToken->save();
                        $countTokens++;
                    }
                    $this->moveSurveyToSurveyLog("Survey {$fromSid} moved to {$currentTo} : {$countTokens} tokens created",1);
                }
            }

            $oFromResponses = \Response::model($fromSid)->findAll(array('order' => 'id'));
            $countResponse = 0;
            foreach($oFromResponses as $oFromResponse) {
                $aResponse = $oFromResponse->getAttributes();
                /* The common columns*/
                $aCommonsColumns = array_intersect_key($aResponse,array_flip($aToCommonsColumns));
                if(!empty($aCommonsColumns['token'])) {
                    $aCommonsColumns['token'] = substr($fromSid.$separator.$aCommonsColumns['token'],-36);
                }
                $oToReponse = \Response::create($currentTo);
                $oToReponse->setAttributes($aCommonsColumns, false);
                /* The columns to be set (NOT NULL) */
                if(empty($aCommonsColumns['startlanguage'])) {
                    $oToReponse->setAttribute('startlanguage',"");
                }
                if(in_array('startdate',$aToCommonsColumns) && empty($aCommonsColumns['startdate'])) {
                    $oToReponse->setAttribute('startdate',date("Y-m-d H:i:s",mktime(0,0,0,1,1,1980)));
                }
                if(in_array('datestamp',$aToCommonsColumns) && empty($aCommonsColumns['datestamp'])) {
                    $oToReponse->setAttribute('datestamp',date("Y-m-d H:i:s",mktime(0,0,0,1,1,1980)));
                }
                /* The questions part */
                foreach($aCommonQuestion as $code => $column) {
                    $toColumn = $aToCodeColumns[$code];
                    $oToReponse->setAttribute($toColumn,$aResponse[$column]);
                }
                /* Save it : need to broke if error …*/
                $oToReponse->save();
                $countResponse++;
            }
            \moveSurveyToSurvey\models\surveyToSurvey::model()->deleteByPk($oSurveyToSurvey->fromsid);
            if($localDirectoryForBackup) {
                $this->backupSurvey($fromSid,$localDirectoryForBackup);
            }
            \Survey::model()->findByPk($fromSid)->delete();
            $this->moveSurveyToSurveyLog("Survey {$fromSid} moved to {$currentTo} : {$countResponse} response moved",1);
        }
    }

    private function backupSurvey($surveyId,$directory)
    {
        $aZIPFileName = $directory.DIRECTORY_SEPARATOR.'survey_archive_'.$surveyId.'.lsa';
        $sLSSFileName = $directory.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR."lss".randomChars(30);
        $sLSRFileName = $directory.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR."lsr".randomChars(30);
        $sLSTFileName = $directory.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR."lst".randomChars(30);
        $sLSIFileName = $directory.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR."lsi".randomChars(30);
        Yii::import('application.libraries.admin.pclzip', true);
        $zip = new PclZip($aZIPFileName);
        touch($sLSSFileName);
        file_put_contents($sLSSFileName, surveyGetXMLData($surveyId));
        $this->addToZip($zip, $sLSSFileName, 'survey_'.$surveyId.'.lss');
        unlink($sLSSFileName);

        if (tableExists("{{survey_".$surveyId."}}")) {
            getXMLDataSingleTable($surveyId, 'survey_'.$surveyId, 'Responses', 'responses', $sLSRFileName, false);
            $this->addToZip($zip, $sLSRFileName, 'survey_'.$surveyId.'_responses.lsr');
            unlink($sLSRFileName);
        }

        if (tableExists("{{tokens_".$surveyId."}}")) {
            getXMLDataSingleTable($surveyId, 'tokens_'.$surveyId, 'Tokens', 'tokens', $sLSTFileName);
            $this->addToZip($zip, $sLSTFileName, 'survey_'.$surveyId.'_tokens.lst');
            unlink($sLSTFileName);
        }

        if (tableExists("{{survey_".$surveyId."_timings}}")) {
            getXMLDataSingleTable($surveyId, 'survey_'.$surveyId.'_timings', 'Timings', 'timings', $sLSIFileName);
            $this->addToZip($zip, $sLSIFileName, 'survey_'.$surveyId.'_timings.lsi');
            unlink($sLSIFileName);
        }
    }

    /**
     * Function copy from export
     * @see \export->_addToZip
     * @param PclZip $zip
     * @param string $name
     * @param string $full_name
     */
    private function addToZip($zip, $name, $full_name)
    {
        $check = $zip->add(
            array(
                array(
                    PCLZIP_ATT_FILE_NAME => $name,
                    PCLZIP_ATT_FILE_NEW_FULL_NAME => $full_name
                )
            )
        );
        if ($check == 0) {
            throw new CException("PCLZip error : ".$zip->errorInfo(true));
        }
    }

    /**
     * Create or update the DB
     */
    private function setDb()
    {
        if (intval($this->get("dbVersion")) >= self::$dbVersion) {
            return;
        }
        $currentDbVersion = $this->get("dbVersion");
        if($currentDbVersion < 2) {
            if($this->api->tableExists($this, 'surveyToSurvey')) {
                Yii::app()->getDb()->createCommand()->renameTable('{{movesurveytosurvey_surveyToSurvey}}', '{{movesurveytosurvey_surveytosurvey}}');
                $this->set("dbVersion", 2);
            } else {
                if (!$this->api->tableExists($this, 'surveytosurvey') ) {
                    $this->api->createTable($this, 'surveytosurvey', array(
                        'fromsid'=>'int not NULL',
                        'tosid'=>'int not NULL',
                    ));
                    //$tableName
                    Yii::app()->getDb()->createCommand()->createIndex('{{idx1_surveytosurveys}}', '{{movesurveytosurvey_surveytosurvey}}', ['fromsid'], true);
                    $this->set("dbVersion", 2);
                }
            }
        }
        $this->set("dbVersion", self::$dbVersion);
    }

    /**
     * Check if this plugin is activated
     */
    private function isActive()
    {
        $oSelfPlugin = Plugin::model()->find("name=:name",array(":name"=>get_class($this)));
        return $oSelfPlugin && $oSelfPlugin->active;
    }
    /**
     * Some compatibility function
     * Survey::model()->hasTokens
     * @param integer $iSurveyId
     * @return boolean;
     */
    private function surveyHasTokens($iSurveyId) {
        Yii::import('application.helpers.common_helper', true);
        return tableExists("{{tokens_".$iSurveyId."}}");
    }

    /**
     * Fix LimeSurvey command function
     */
    private function LsCommandFix()
    {
        /* Potential bad autoloading in command */
        include_once(dirname(__FILE__)."/DbStorage.php");
        // These take care of dynamically creating a class for each token / response table.
        /* TODO check if not already registered */
        Yii::import('application.helpers.ClassFactory');
        if(!function_exists('gT') || !function_exists('quoteText')) {
            /* 4.X issue : Error: Call to undefined function gT() in application/models/SurveysGroupsettings.php:403 */
            Yii::import('application.helpers.common_helper', true);
           
        }
        ClassFactory::registerClass('Token_', 'Token');
        ClassFactory::registerClass('Response_', 'Response');
        $defaulttheme = Yii::app()->getConfig('defaulttheme');
        /* Bad config set for rootdir */
        if( !is_dir(Yii::app()->getConfig('standardthemerootdir').DIRECTORY_SEPARATOR.$defaulttheme) && !is_dir(Yii::app()->getConfig('userthemerootdir').DIRECTORY_SEPARATOR.$defaulttheme)) {
            /* This included can be broken */
            $webroot = (string) Yii::getPathOfAlias('webroot');
            /* Switch according to LimeSurvey version */
            $configFixed = array(
                'standardthemerootdir'   => $webroot.DIRECTORY_SEPARATOR."templates",
                'userthemerootdir'       => $webroot.DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."templates",
            );
            if(intval(Yii::app()->getConfig('versionnumber')) > 2) {
                $configFixed = array(
                    'standardthemerootdir'   => $webroot.DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR."survey",
                    'userthemerootdir'       => $webroot.DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR."survey",
                );
            }
            $configConfig = require (Yii::getPathOfAlias('application').DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');
            if (isset($configConfig['config'])) {
                $configFixed = array_merge($configFixed, $configConfig['config']);
            }
            $aConfigToFix = array(
                'standardthemerootdir',
                'userthemerootdir',
            );
            foreach($aConfigToFix as $configToFix) {
                Yii::app()->setConfig($configToFix,$configFixed[$configToFix]);
            }
        }
    }

    /**
     * Check if a directory is valid for purpose
     * @return false|string
     */
    private function checkLocalDirectory($localDirectory)
    {
        /* Remove the warning about open_base_dir */
        $localDirectory = @realpath($localDirectory);
        if($localDirectory === false) {
            $this->errorOnDir = $this->gT("The directory you set is invalid and can not be used.");
            return false;
        }
        rmdirr($localDirectory."/backupSurveyLocallyCheck");
        if(!mkdir($localDirectory."/backupSurveyLocallyCheck")) {
            $this->errorOnDir = $this->gT("Unable to create directory in directory set.");
            return false;
        }
        if(!touch($localDirectory."/backupSurveyLocallyCheck/file")) {
            $this->errorOnDir = $this->gT("Able to create directory in directory set, but unable to create a file in the directory created.");
            return false;
        }
        if(!rmdirr($localDirectory."/backupSurveyLocallyCheck")) {
            $this->errorOnDir = $this->gT("Able to create directory in directory set, but unable to delete.");
            return false;
        }
        if(!is_dir($localDirectory.DIRECTORY_SEPARATOR."tmp")) {
            mkdir($localDirectory.DIRECTORY_SEPARATOR."tmp");
        }
        return $localDirectory;
    }

    /**
    * log an event
    * @param string $sLog
    * @param int $state
    * retrun @void
    */
    private function moveSurveyToSurveyLog($sLog,$state=0){
        // Play with DEBUG : ERROR/LOG/DEBUG
        $sNow=date(DATE_ATOM);
        switch ($state){
            case 0:
                $sLevel='error';
                $sLogLog="[ERROR] $sLog";
                break;
            case 1:
                $sLevel='info';
                $sLogLog="[INFO] $sLog";
                break;
            case 2:
                $sLevel='info';
                $sLogLog="[DEBUG] $sLog";
                break;
            default:
                $sLevel='trace';
                $sLogLog="[TRACE] $sLog";
                break;
        }
        Yii::log($sLog, $sLevel,'plugin.moveSurveyToSurvey');
        if($state <= $this->debug || $state==0)
        {
            echo "[{$sNow}] {$sLogLog}\n";
        }
    }

}
