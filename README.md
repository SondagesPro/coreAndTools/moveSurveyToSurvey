# moveSurveyToSurvey

A CLI tool to move ssurvey(s) to another survey(s).

## Installation

This plugin need [getQuestionInformation](https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation) pluginn installed.

It can be [installed and activated like any other plugin](https://extensions.sondages.pro/about/install-and-activate-a-plugin-for-limesurvey.html).

## Usage

After installation : a DB table was created in you database : `{prefix}surveyToSurvey`, with default prefix : `lime_surveyToSurvey`.
This table have 2 columns : `fromsid` and `tosid`.

In plugin setting : you can copy paste the 2 columns : 1st column is the from survey id and second column are the to survey id. Separator can be `[tab]` or `;`. You can directly copy paste 2 columns of libre office calc.

Purpose of this survey is to move data form `fromsid` to `tosid`. The copy are done using question code correspondance, no control of data validity is done.

If you set a backup directory, the from survey are deleted and saved a lsa in this directory.

Finally the action to do via PHP cli. Inside LimeSurvey directory. was `php application/commands/console.php plugin --target=moveSurveyToSurvey`
