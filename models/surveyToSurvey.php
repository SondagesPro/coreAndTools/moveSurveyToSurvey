<?php
/**
 * This file is part of moveSurveyToSurvey plugin
 * @version 0.1.0
 */
namespace moveSurveyToSurvey\models;

use Yii;
use CActiveRecord;

class surveyToSurvey extends CActiveRecord
{
    /**
     * Class surveyChaining\models\chainingResponseLink
     *
     * @property integer $fromsid from survey
     * @property integer $tosid to survey
    */

    /** @inheritdoc */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    /** @inheritdoc */
    public function tableName()
    {
        return '{{movesurveytosurvey_surveytosurvey}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return 'fromsid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $aRules = array(
            array('fromsid', 'required'),
            array('fromsid', 'unique'),
            array('tosid', 'required'),
            array('fromsid,tosid', 'numerical', 'integerOnly'=>true),
        );
        return $aRules;
    }
}
